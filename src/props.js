export default {
  testCategory: [
    ['DEV', 'Developer'],
    ['DEV_UI','UI Developer'],
    ['DEV_BE','BE Developer'],
    ['QA','Quality Assuarance'],
    ['SM','Scrum Master'],
    ['HR','Human Resources'],
    ['MAN','Manager'],
    ['OTHER','Other'],
  ],
}