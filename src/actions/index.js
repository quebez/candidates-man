import axios from 'axios';
import { withRouter } from 'react-router-dom';
import React from 'react';

import {
	AUTH_USER, UNAUTH_USER, AUTH_ERROR, FETCH_INVITATIONS,
	INVITATION_SAVED, INVITATION_FAILED, INVITATION_DELETED
} from './types';
import { history } from '../app.js';

const ROOT_URL = 'http://localhost:3090';


export function login({ email, password }) {
	return function (dispatch) {      //this function executed by middleware
		axios.post(`${ROOT_URL}/manager`, { email, password })
			.then(response => {
				// Update state to indicate manager is authenticated
				dispatch({ type: AUTH_USER });

				// Save JWT token
				localStorage.setItem('token', response.data.token);

				// Redirect user
				history.push('/invitations');
			})
			.catch(error => {
				console.log(error.response)
				if (error.response.status === 404) {
					dispatch(authError('Bad Credentials'));
				} else if (error.response.status === 400) {
					dispatch(authError(error.response.data.error));
				} else {
					dispatch(authError('Something really bad happen'))
				}
			});
	}
}

export function authError(error) {
	return {
		type: AUTH_ERROR,
		payload: error
	};
}

export function logout() {
	localStorage.removeItem('token');

	return {
		type: UNAUTH_USER
	}
}

export function fetchInvitaitons(deleted = false) {
	return function (dispatch) {
		axios.get(
			`${ROOT_URL}/manager/invitation`,
			{ headers: { authorization: localStorage.getItem('token') } }
		).then(response => {
			dispatch({
				type: FETCH_INVITATIONS,
				payload: response.data,
				deleted
			});
		}).catch(error => {
			if (error.response.status === 404) {
				//TODO: Add error
			}
		});
	}
}

export function invite({ email, name, testCategory, daysValid }) {
	return function (dispatch) {
		axios.post(
			`${ROOT_URL}/manager/invitation`,
			{ email, name, testCategory, daysValid },
			{ headers: { authorization: localStorage.getItem('token') } }
		).then(response => {
			dispatch({
				type: INVITATION_SAVED
			});
		}).catch(error => {
			dispatch({
				type: INVITATION_FAILED,
				payload: error.response.data.error
			})
		});
	}
}

export function deleteInvitation(id) {
	return function (dispatch) {
		axios.delete(
			`${ROOT_URL}/manager/invitation/${id}`,
			{ headers: { authorization: localStorage.getItem('token') } }
		).then(response => {
			dispatch(fetchInvitaitons(true));
		}).catch(error => {
			dispatch({
				type: INVITATION_FAILED,
				payload: error.response.data.error
			})
		});
	}
}

/* 
    JWT token is saved a local storage and no other website has access to it
    Whenever user visits the website again, the local storage still has the token
    Manager token wont expire, 
    Candidate token expires in timeForTest + someAmountOfTime just to be sure he saves the test
*/