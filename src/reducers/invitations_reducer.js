import { FETCH_INVITATIONS, INVITATION_SAVED, INVITATION_FAILED, INVITATION_DELETED } from '../actions/types';

export default function(state = {}, action) {
    switch(action.type) {
        case FETCH_INVITATIONS:
            return { ...state, invitations: action.payload, deleted: action.deleted };
        case INVITATION_SAVED:
            return { ...state, submitted: true };
        case INVITATION_FAILED:
            return { ...state, submitted: false, error: action.payload };
    }
    
    return state;
}