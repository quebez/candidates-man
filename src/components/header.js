import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

class Header extends Component {
    renderLinks(links) {
        return links.map(link => {
            return (
            <li className="nav-item" key={link}>
                <Link className="nav-link" to={link}>{link.charAt(0).toUpperCase() + link.slice(1)}</Link>
            </li>
            )
        });
    }

    render() {
        const links = this.props.authenticated ? ['logout', 'invitations', 'templates', 'tests'] : ['login'];

        return(
                <nav className="navbar navbar-light">
                    <Link to="/" className="navbar-brand">Candidates</Link>
                    <ul className="nav navbar-nav flex-row-reverse">
                        {this.renderLinks(links)}
                    </ul>
                </nav>
        ); 
    }
}

function mapStateToProps(state) {
    return { authenticated: state.auth.authenticated };
}

export default connect(mapStateToProps)(Header);