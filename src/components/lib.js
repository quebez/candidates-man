import React from 'react';
import { Field } from 'redux-form';

export default {
  renderFormField(name, type='text', placeholder) {
    return (
      <fieldset className="form-group">
        <label>
          {name.charAt(0).toUpperCase() + name.slice(1)}:
        </label>
        <Field
          name={name}
          component="input"
          type={type}
          className="form-control"
          placeholder={placeholder}
        />
      </fieldset>
    )
  },

  renderFormDropdown(label, options) {
    return (
      <fieldset className="form-group">
      <label htmlFor={label}>
        {label.charAt(0).toUpperCase() + label.slice(1)}:
      </label>
      <Field className="form-control" id={label} component="select" name={label}>
        <option></option>
        {options.map(arr => getOption(arr[0], arr[1]))}
      </Field>
    </fieldset>
    )
  },

  renderErrors(errors) {
    let output = ' ';
  
    for (let error in errors) {
      output = output + errors[error] + ' '; 
    }
    
    return (
      <div className="alert alert-danger">
        <strong>Error!</strong>
        {output}
      </div>
    );
  }
}

function getOption(value, option) {
  return <option value={value} key={value}>{option}</option>
}