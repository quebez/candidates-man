import React, { Component } from 'react';
import { connect } from 'react-redux';
import moment from 'moment';
import { Link } from 'react-router-dom';

import * as actions from '../actions';


class Invitations extends Component {
	componentWillMount() {
		this.props.fetchInvitaitons();
	}

	// componentWillUpdate() {
	// 	this.props.fetchInvitaitons();
	// }

	handleDelete(obj) {
		this.props.deleteInvitation(obj.target.id);
	}

	render() {
		if (!this.props.invitations) {
			return <Link className="btn btn-primary" to="/invitation">Add Invitation</Link>
		}

		const invitationDeletedSucc = this.props.invitationDeleted ? 
			<div className="alert alert-success">
				<strong>Sucess!</strong>
			</div> : 
			<div>

			</div>

		const invitations = this.props.invitations.map(invitation => {
			const { _id, email, name, expiration, testCategory } = invitation;

			const expiredColor = Date.now() > expiration
				? { backgroundColor: 'red' }
				: { backgroundColor: 'green' };
			const expirationDate = moment(expiration).format('MMMM Do YYYY, h:mm:ss a');

			return (
				<li className="col-md-6 list-group-item" key={_id}>
					<ul className="list-group">
						<li className="list-group-item"><strong>{name}</strong></li>
						<li className="list-group-item link">
							<a href={`mailto:${email}`}>{email}</a>
						</li>
						<li className="list-group-item">
							<span className='circle' style={expiredColor}></span>
							{' '}<strong>Expiration:</strong> {expirationDate}
						</li>
						<li className="list-group-item"><strong>Category:</strong> {testCategory}</li>
						<li className="list-group-item"><strong>ID:</strong> {_id}</li>
						<button 
							className="btn btn-danger"
							id={_id}
							onClick={this.handleDelete.bind(this)}
						>Delete</button>
					</ul>
				</li>
			);
		});

		return (
			<div className="container">
				<Link className="btn btn-primary" to="/invitation">Add Invitation</Link>
				{invitationDeletedSucc}
				<ul className="list-group d-flex flex-row flex-wrap">
					{invitations}
				</ul>
			</div>
		);
	}
}

function mapStateToProps(state) {

	console.log(state.invitations);
	return { 
		invitations: state.invitations.invitations,
		invitationDeleted: state.invitations.deleted
	}
}

export default connect(mapStateToProps, actions)(Invitations);