import React, { Component } from 'react';
import { Field, reduxForm } from 'redux-form';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

import * as actions from '../actions';
import lib from './lib';
import props from '../props';

class Invitation extends Component {
  handleFormSubmit({ email, name, category, daysValid }) {
    this.props.invite({ email, name, testCategory: category, daysValid });
  }


  render() {
    const { handleSubmit, pristine, submitting, validationMessages, 
        submitFailed, invitationSaved, submissionMessage } = this.props;
    let errors = '';
    if (submitFailed && validationMessages) errors = lib.renderErrors(validationMessages)
    else if (!invitationSaved && submissionMessage) errors = lib.renderErrors({error: submissionMessage})

    const success = invitationSaved ? 
      <div className="alert alert-success">
        <strong>Sucess! </strong> Invitation was successfully saved.
      </div> : '';

    return (
      <form onSubmit={handleSubmit(this.handleFormSubmit.bind(this))}>
        {lib.renderFormField('email')}
        {lib.renderFormField('name')}
        {lib.renderFormDropdown('category', props.testCategory)}
        {lib.renderFormField('daysValid', 'number', 'default 60 days')}
        { success }
        { errors }
        <button action="submit" className="btn btn-primary" disabled={pristine || submitting}>
          Submit
        </button>
        <Link className="btn btn-danger" to="/invitations" disabled={submitting}>
          Back
        </Link>
      </form>
    );
  }
}

function validate(values) {
  const errors = {};
  
  if (!values.email) errors.email = "Enter email.";
  if (!values.name) errors.name = "Enter name.";
  if (!values.category) errors.category = "Select test category.";

  return errors;
}

function mapStateToProps(state) {
  return { 
    validationMessages: state.form.invitation.syncErrors,
    invitationSaved: state.invitations.submitted,
    submissionMessage: state.invitations.error
  };
}

export default reduxForm({
  validate,
  form: 'invitation',
})(
  connect(mapStateToProps, actions)(Invitation)
);