export const AUTH_USER = 'auth_user';
export const UNAUTH_USER = 'unauth_user';
export const AUTH_ERROR = 'auth_error';

export const FETCH_INVITATIONS = 'fetch_invitations';
export const INVITATION_SAVED = 'invitation_saved';
export const INVITATION_FAILED = 'invitation_failed';
export const INVITATION_DELETED = 'invitation_deleted';