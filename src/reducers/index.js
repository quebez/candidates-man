import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';
import authReducer from './auth_reducer';
import invitationsReducer from './invitations_reducer';

const rootReducer = combineReducers({
    form: formReducer,
    auth: authReducer,
    invitations: invitationsReducer
});

export default rootReducer;