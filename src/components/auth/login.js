import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Field, reduxForm } from 'redux-form';

import * as actions from '../../actions';
import lib from '../lib';

class Login extends Component {
	handleFormSubmit({ email, password }) {
		this.props.login({ email, password });
	}

	renderAlert() {
		if (this.props.errorMessage) {
			return (
				<div className="alert alert-danger">
					<strong>Oops!</strong> {this.props.errorMessage}
				</div>
			);
		}
	}

	render() {
		const { handleSubmit } = this.props;

		return (
			<form onSubmit={handleSubmit(this.handleFormSubmit.bind(this))}>
				{lib.renderFormField('email')}
				{lib.renderFormField('password', 'password')}
				{this.renderAlert()}
				<button action="submit" className="btn btn-primary">
					Log in
        </button>
			</form>
		);
	}
}

function mapStateToProps(state) {
	return { errorMessage: state.auth.error };
}

export default reduxForm({
	form: 'login',
})(
	connect(mapStateToProps, actions)(Login)
);