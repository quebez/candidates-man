import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import { Router, Route, IndexRoute } from 'react-router-dom';
import reduxThunk from 'redux-thunk';
import createBrowserHistory from 'history/createBrowserHistory'

import './scss/main.scss';
import App from './components/app';
import reducers from './reducers';
import Login from './components/auth/login';
import Logout from './components/auth/logout';
import RequireAuth from  './components/auth/require_auth';
import Invitations from './components/invitations';
import Invitation from './components/invitation';
import { AUTH_USER } from './actions/types';

export const history = createBrowserHistory();

const createStoreWithMiddleware = applyMiddleware(reduxThunk)(createStore);
const store = createStoreWithMiddleware(reducers);
const token = localStorage.getItem('token');

if (token) {
  store.dispatch({ type: AUTH_USER });
}

ReactDOM.render(
  <Provider store={store}>
    <Router history={history}>
      <App>
        <Route path="/login" component={Login} />
        <Route path="/logout" component={Logout} />
        <Route path="/invitations" component={RequireAuth(Invitations)} />
        <Route path="/invitation" component={RequireAuth(Invitation)} />
      </App>
    </Router>
  </Provider>
  , document.querySelector('.container'));

  /* */